# -*- coding: UTF-8 -*-
import sys

nombre_tabla=""
persistencia=""
aplicativo=""
sistema_origen=""
frecuencia=""
uuaa=""
archivoSQL=""
numero_campos=0
esquema_teradata=""
campos_formato=[]

def validaArgumentos(argumentosLen):
    global nombre_tabla
    global persistencia
    global aplicativo
    global sistema_origen
    global frecuencia
    global uuaa
    global archivoSQL
    global numero_campos
    global esquema_teradata
    global campos_formato
    argumentos = sys.argv
    if argumentosLen>1:
        i=0;
        contador=0
        #inicializa las banderas para obtener argumentos duplicados
        nombre_tabla_bandera=False
        sistema_origen_bandera=False
        aplicativo_bandera=False
        uuaa_bandera=False
        persistencia_bandera=False
        # Itera los argumentos introducidos.
        for argumento in argumentos:
            result=True;
            if i%2>0:
                #analiza el argumento impar y lo compara con los diferentes prefijos de entrada
                result=validaArgumento(argumento)
                if result==False:
                    if argumentosLen<=i+1:
                        result=validaArgumento(argumento)
                        if result ==False:
                            print("--> No se reconoce un prefijo:")
                            printArgumentos()
                    else:
                        print ("--> Faltan argumentos o no se reconoce un prefijo: ")
                        printArgumentos()
                else:
                    #Si hay menos de un argumento
                    if argumentosLen<=i+1:
                        print ("--> Faltan argumentos: ")
                        printArgumentos()
            else:
                if i>1:
                    argumento2=str(argumentos[i-1].strip())
                    result=validaArgumento(argumento2)
                    if result ==False:
                        print("--> No se reconoce un prefijo:")
                        printArgumentos()
                    else:
                        # Inicializa las variables si es que encuentra el prefijo correspondiente.
                        if argumento2 == "-t":
                            nombre_tabla=argumento
                            contador+=1
                            if nombre_tabla_bandera == False:
                                nombre_tabla_bandera=True
                            else:
                                print("--> Parametro duplicado -t")
                                printArgumentos()
                        if argumento2 == "-s":
                            sistema_origen=argumento
                            contador+=1
                            if sistema_origen_bandera == False:
                                sistema_origen_bandera=True
                            else:
                                print("--> Parametro duplicado -s")
                                printArgumentos()
                        if argumento2 == "-p":
                            persistencia=argumento
                            contador+=1
                            if persistencia_bandera == False:
                                persistencia_bandera=True
                            else:
                                print("--> Parametro duplicado -p")
                                printArgumentos()
                        if argumento2 == "-f":
                            frecuencia=argumento
                        if argumento2 == "-a":
                            aplicativo=argumento
                            contador+=1
                            if aplicativo_bandera == False:
                                aplicativo_bandera=True
                            else:
                                print("--> Parametro duplicado -a")
                                printArgumentos()
                        if argumento2 == "-u":
                            uuaa=argumento
                            contador+=1
                            if uuaa_bandera == False:
                                uuaa_bandera=True
                            else:
                                print("--> Parametro duplicado -u")
                                printArgumentos()
                        if argumento2 == "-sql":
                            archivoSQL=argumento
                        if argumento2 == "-st":
                            esquema_teradata=argumento
            i+=1
            #si encuentra menos de 5 argumentos y es diferente a teradata solicita argumentos faltantes
        if contador < 5 and sistema_origen.lower()!="teradata":
            print("--> Faltan argumentos mínimos: ")
            printArgumentos()
        else:
            # si sistema es igual a teradata y esquema esta vacío entoces solicita el argumento -st
            if sistema_origen.lower()=="teradata" and esquema_teradata=="":
                print("--> Faltan argumentos mínimos, el sistema es Teradata: ")
                printArgumentos()
    else:
        # si los argumentos son menores que 1 entonces solicita lo argumentos
        print("--> No se encontraron argumentos... ")
        printArgumentos()
    ## función para validar los prefijos
def validaArgumento(argumento):
    switcher = {
    "-t": True,
    "-s": True,
    "-p": True,
    "-f": True,
    "-a": True,
    "-u": True,
    "-sql": True,
    "-st": True,
    }
    result=switcher.get(argumento,False)
    return result
    #función que imprime los argumentos necesarios.
def printArgumentos():
    print("--> Los argumentos permitidos son: \n -t nombre_tabla (obligatorio) \n -s sistema_origen (obligatorio) \n -p persistencia (obligatorio)\n -f frecuencia \n -a aplicativo (obligatorio)\n -u uuaa \n -sql archivoSQL \n -st esquemaTeradata (solo cuando es sistema Teradata)")
    sys.exit()
    # valida la persistencia
def validaPersistencia(persis):
    switcher = {
    "raw": True,
    "master": True,
    }
    result=switcher.get(persis,False)
    if result=="":
		print("--> No se ha introducido presistencia. \n El programa se cerrará.")
		printArgumentos()
		sys.exit
    else:
        if result== False:
            print("--> Valor de Persistencia no válido (raw master). \n El programa se cerrará.")
            sys.exit
    return result
    #valida sistema origen
def validaSistemaOrigen(sistema_origen):
    switcher = {
    "oracle": True,
    "teradata": True,
    "host": True,
    }
    result=switcher.get(sistema_origen.lower(),False)
    if result=="":
        print("--> No se ha introducido Sistema Origen (oracle, teradata, host). \n El programa se cerrará.")
        printArgumentos()
        sys.exit
    else:
        if result== False:
            print("--> Valor de Sistema Origen no válido (oracle, teradata, host). \n El programa se cerrará.")
            sys.exit
    return result
    # valida las frecuencias, si estan en español las tranforma a inglés.
def validaFrecuencia(frecuencia):
    switcher = {
    "Diaria": "Daily",
    "Mensual": "Monthly",
    "Quincenal": "Biweekly",
    "Semanal": "Weekly",
    "Bimestral": "Biweekly",
    "Daily": True,
    "Weekly": True,
    "Monthly": True,
    "Bimonthly": True,
    "Biweekly": True,
    }
    result=switcher.get(frecuencia.capitalize(),False)
    if result=="":
        print("--> No se ha introducido la frecuencia (Ejemplo: Mensual, Quincenal, Semanal, Daily, Weekly, Monthly, Bimonthly, Bimestral, Biweekly)")
        print("--> Se continuará con la ejecución pero deberá indicarse en el diccionario una vez generado.")
    else:
        if result== False:
            print("--> No se encuentra la frecuencia. Se inicializará a Null, pero se tendrá que modificar en el diccionario una vez generado.")
    return result
    # si está indicado lee el archivo de text que contiene el sql de la vista o tabla 
    # de aquí se esperan obtener los diferentes campos y formatos del origen.
def readFileSQL():
    try:
        f=open("SQL.txt", "r")
        if f.mode == 'r':
            content=f.read().strip()
            content=content.replace("\n","")
        print("Leyendo archivo... Tamaño: "+str(len(content)))
        if len(content)>0:
            campos=[]
            nombre_campos=[]
            result = []
            i=0
            while i<len(content):
                date=content.find("DATE",i,len(content))
                varchar=content.find("VARCHAR",i,len(content))
                char=content.find(" CHAR(",i,len(content))
                integer=content.find("INTEGER",i,len(content))
                decimal=content.find("DECIMAL",i,len(content))
                byteint=content.find("BYTEINT",i,len(content))
                inicializar=[date,varchar,char,integer,decimal,byteint]
                i=inicializar[0]
                j=0
                if(date==-1 and varchar ==-1 and char==-1 and integer==-1 and decimal==-1 and byteint==-1):
                    i=len(content)
                while j<len(inicializar):
                    if inicializar[j] !=-1:
                        aux=inicializar[j]
                        if i>aux:
                            i=aux
                        else:
                            if(i==-1):
                                i=aux
                    j+=1
                if i == date:
                    i+=5
                    if content[date+5]=="F" :
                        campos.append(content[date:date+4])
                        aux=-2
                        while content[date+aux]!=" ":
                            aux-=1
                        aux+=1
                        nombre_campos.append(content[date+aux:date-1])
                if i == varchar:
                    if content[varchar+10]==")":
                        campos.append(content[varchar:varchar+11])
                    if content[varchar+9]==")":
                        campos.append(content[varchar:varchar+10])
                    if content[varchar+11]==")":
                        campos.append(content[varchar:varchar+12])
                    i+=7
                    aux=-2
                    while content[varchar+aux]!=" ":
                        aux-=1
                    aux+=1
                    nombre_campos.append(content[varchar+aux:varchar-1])
                if i == integer:
                    campos.append(content[integer:integer+8])
                    i+=7
                    aux=-2
                    while content[integer+aux]!=" ":
                        aux-=1
                    aux+=1
                    nombre_campos.append(content[integer+aux:integer-1])
                if i == decimal:
                    if content[decimal+11]==")":
                        campos.append(content[decimal:decimal+12])
                        aux=-2
                        while content[decimal+aux]!=" ":
                            aux-=1
                        aux+=1
                        nombre_campos.append(content[decimal+aux+1:decimal-1])
                    if content[decimal+10]==")":
                        campos.append(content[decimal:decimal+10])
                        aux=-2
                        while content[decimal+aux]!=" ":
                            aux-=1
                        aux+=1
                        nombre_campos.append(content[decimal+aux+1:decimal-1])
                    if content[decimal+12]==")":
                        campos.append(content[decimal:decimal+13])
                        aux=-2
                        while content[decimal+aux]!=" ":
                            aux-=1
                        aux+=1
                        nombre_campos.append(content[decimal+aux:decimal-1])
                    i+=7
                if i == byteint:
                    campos.append(content[byteint:byteint+8])
                    i+=7
                if i == char:
                    if content[char+7]==")":
                        campos.append(content[char+1:char+8])
                    if content[char+8]==")":
                        campos.append(content[char+1:char+9])
                    if content[char+9]==")":
                        campos.append(content[char+1:char+10])
                    i+=6
                    aux=-1
                    while content[char+aux]!=" ":
                        aux-=1
                    aux+=1
                    nombre_campos.append(content[char+aux:char-1])
            campos_formato=[]
            campos_formato.append(campos)
            campos_formato.append(nombre_campos)
            return campos_formato
        else:
            campos_formato=[]
            return campos_formato
    except:
        print("--> Error al intentar leer el archivo SQL.txt en la raiz.")
        campos_formato=[]
        return campos_formato
def validaArchivoSQL(archivoSQL):
    global campos_formato
    global numero_campos
    if archivoSQL.lower()=='f' or archivoSQL.lower()=='t' or archivoSQL.lower()=='':
        if archivoSQL.lower()=='f' or archivoSQL.lower()=='':
                    try:
                        numero_campos=int(raw_input("Dado que no se indicó archivoSQL debe introducir un número de campos. \nIndica el número de campos: "))
                        if numero_campos<=0:
                                print("Se espera un número mayor a 0. \n El programa se cerrará.")
                    except:
                        print("Se espera un número entero, se cerrará el programa.")
                        sys.exit()
        if archivoSQL.lower()=='t':
                campos_formato=readFileSQL()
		if len(campos_formato)==2:
                        if len(campos_formato[0])>0:
                                print("Se encontraron: " + str(len(campos_formato[0]))+" campos.")
                                numero_campos=len(campos_formato[0])
                        else:
                                print("No se encontrarón campos.")
                                sys.exit()
    else:
            print("Valor en parametro SQL no encontrado... \n El programa se cerrará")
            sys.exit
def imprimirVariables():
    print("Número de campos: " +str(numero_campos))
    print("Frecuencia: " +frecuencia)
    print("Persistencia: "+persistencia)
    print("Sistema origen: "+sistema_origen)
    print("Esquema teradata: "+esquema_teradata)
    print("UUAA: "+uuaa)
    print("Aplicativo: "+aplicativo)
    print("Nombre de la tabla: " +nombre_tabla)
