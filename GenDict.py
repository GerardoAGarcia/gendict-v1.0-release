# -*- coding: UTF-8 -*-
from __future__ import print_function
import pickle
from pprint import pprint
from googleapiclient import discovery
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import sys
import module_object as mo
import module_validation as mv
import module_file as mf

def main():
    SCOPES = ['https://www.googleapis.com/auth/drive']
    PLANTILLA_ID = '1teHqWMaBiiWmCLJ0pbiL0MDYns3Gtq8CxDq__bP8Ilg'
    SHEET_ID1 = 1159459767
    SHEET_ID2 = 218187839
    SHEET_ID3 = 1079943955
    SPREADSHEET_DATASOURCE = '12pLx9LJSUo-_McudsV7X_Ejy0wSRJrUpB1uJNth8VTQ'
    RANGE_DATASOURCE = 'OLD DS Map!A6:I15'
    SPREADSHEET_ALHAMBRA = '1wZ6IU_CtnQPsi8v6z76dY6oeBZ_Xb7nRvoITOTPAd14'
    RANGE_ALHAMBRA = 'Estructura de directorios!E2:I261'
    SPREADSHEET_BUI = '1SFjlQaCdb7zPklxq9KvyCiksH9-hdo3bZT2QjvotRpE'
    RANGE_BUI = 'Base Final!B4:AH2348'
    SPREADSHEET_PI11 = '1o6OSwyF73BBHM6hNMKIS58H8Bwc7aOe6ZyBFuwgMp78'
    RANGE_NAME_PI11 = 'DC-DD-Field-RAW!A5:AH7150'
    SPREADSHEET_PI10 = '1spu3h--hBLStsz3HvRqVvZUYooGLSSxib3OuHgxHsik'
    RANGE_NAME_PI10 = 'DC-DD-Field-RAW!A5:AA4100'
    SPREADSHEET_PI9 = '1J5sH5tS6lfdnZUIncTc9b20WFORE2Z7yTI_MJF0gInE'
    RANGE_NAME_PI9 = 'DC-DD-Field-RAW!A5:W8475'
    ID_GLOBAL = '1VKV_5Q-GeVwXLwG8Q7_6MNAapZDkWHTFApTO_72RUog'
    ID_GLOBAL = '1VKV_5Q-GeVwXLwG8Q7_6MNAapZDkWHTFApTO_72RUog'
    RANGE_GLOBAL='GLOBAL - Central Repository of Namings!B5:G'
    mf.banner()
    credentials = mf.getCredentials(SCOPES)
    service = discovery.build('sheets', 'v4', credentials=credentials)
    mv.validaArgumentos(len(sys.argv))
    valorPersistencia=mv.validaPersistencia(mv.persistencia.lower())
    valorSistemaOrigen=mv.validaSistemaOrigen(mv.sistema_origen.lower())
    valorFrecuencia=mv.validaFrecuencia(mv.frecuencia)
    valorArquivoSQL=mv.validaArchivoSQL(mv.archivoSQL)
    mv.imprimirVariables()
    infoBUI=mf.getTablaBUI(service,mv.nombre_tabla,SPREADSHEET_BUI,RANGE_BUI)

    objectA="Mexico"
    objectB=mo.getNombreFiscoObjeto(mv.persistencia.upper(),mv.sistema_origen,mv.nombre_tabla,mv.aplicativo,mv.uuaa)
    objectC="NOMBRE LOGICO"
    objectD="DESCRIPCION"
    objectE="AGRUPACION FUNCIONAL NIVEL 1"
    objectF="AGRUPACION FUNCIONAL NIVEL 2"
    objectG=""#PERÍMETRO
    objectH=""#NIVEL INFORMACIÓN
    dataSource=mo.getDataSourceResponsable(service,SPREADSHEET_DATASOURCE,RANGE_DATASOURCE,mv.uuaa.upper())
    if dataSource[0]==True:
		objectI=dataSource[1]#DATA SOURCE (DS)
		objectJ=dataSource[2]#RESPONSABLE TÉCNICO
    else:
		objectI="No sé encontró"#DATA SOURCE (DS)
		objectJ="No se encontró"#RESPONSABLE TÉCNICO
    tipoZonaArchivo=mo.getAlmacenamientoTipoZonaArchivoSalida(mv.persistencia.upper())
    objectK=tipoZonaArchivo[0]#TIPO ALMACENAMIENTO
    objectL=tipoZonaArchivo[1]#ZONA DE ALMACENAMIENTO
    objectM="File"#TIPO OBJETO
    objectN=mo.getRutaEsquema(service,mv.persistencia,SPREADSHEET_ALHAMBRA,RANGE_ALHAMBRA,mv.uuaa,mv.aplicativo,objectB)#RUTA/ESQUEMA
    objectO=mv.uuaa.upper()#CÓDIGO DE SISTEMA/UUAA
    objectP=""#PARTICIONES
    objectQ=mv.frecuencia
    if infoBUI[0]==True:
        try:
            objectT=infoBUI[4]
            if mv.persistencia.lower() == "raw":
                objectX= infoBUI[1]
            else:
                objectX=mo.getNombreFiscoObjeto("RAW",mv.sistema_origen,mv.nombre_tabla,mv.aplicativo,mv.uuaa)
        except:
            objectT=""
            if mv.persistencia.lower() == "raw":
                objectX="No sé encontró nombre origen"
            else:
                objectX=mo.getNombreFiscoObjeto("RAW",mv.sistema_origen,mv.nombre_tabla,mv.aplicativo,mv.uuaa)
            objectX=""#NOMBRE FÍSICO OBJETO ORIGEN
    else:	
        objectT=""#PROFUNDIDAD ACTUAL
        objectX=""
    objectR=""#TIMING GENERACIÓN REQUERIDO
    objectS=""#TIPO CARGA
    objectU=""#PROFUNDIDAD REQUERIDA
    objectV=""#VOLUMETRÍA ESTIMADA
    objectW=mv.sistema_origen.upper()#SISTEMA ORIGEN
    objectY=""#CONTACTO ORIGEN
    objectZ=mo.getRutaOrigen(mv.persistencia.upper(),mv.sistema_origen.upper(),mv.aplicativo.lower(),mv.nombre_tabla,mv.esquema_teradata)#RUTA ORIGEN
    objectAA=mo.getRutaEsquemasCurrent(mv.persistencia.upper(),mv.aplicativo,mv.uuaa)#RUTA ESQUEMA=
    objectAB=""#TIPO DE ARCHIVO DE ENTRADA=
    objectAC=""#DELIMITADOR DEL ARCHIVO DE ENTRADA=
    objectAD=tipoZonaArchivo[2]#TIPO DE ARCHIVO DE SALIDA=
    objectAE=""#DELIMITADOR DEL ARCHIVO DE SALIDA=
    objectAF=""#VALIDADO POR DATA ARCHITECT=
    objectAG=""#FECHA DE ALTA=
    objectAH=mo.getEtiqueta(mv.nombre_tabla)#ETIQUETAS=
    objectAI="NO"#MARCA OBJETO TÁCTICO
    nombre_diccionario = "t"+"_"+mv.uuaa.lower()+"_"+objectB.lower()+"_"+mv.persistencia.lower()
    valuesDC_DD_Object=[
		objectA,objectB,objectC,objectD,objectE,objectF,objectG,objectH,objectI,objectJ,objectK,objectL,objectM,
		objectN,objectO,objectP,objectQ,objectR,objectS,objectT,objectU,objectV,objectW,objectX,objectY,objectZ,objectAA,
		objectAB,objectAC,objectAD,objectAE,objectAF,objectAG,objectAH,objectAI
	]
    valuesDC_DD_Field = []
    i=0
    fieldO = objectX
    tipoDato=""
    while i<int(mv.numero_campos):
        if mv.archivoSQL.lower() == "t":
    		nombre_campo = mv.campos_formato[1][i]
    		formato_campo= mv.campos_formato[0][i]
        else:
            nombre_campo=""
            formato_campo=""
        aux=i+5
        data = ["Mexico", #1
    	valuesDC_DD_Object[1], #2
    	valuesDC_DD_Object[10], #3
    	valuesDC_DD_Object[11],#4
    	"",#5
    	"=BUSCARV($E"+str(aux)+";'Hoja 1'!$B:$D;2;FALSO())", #6
    	"=BUSCARV($E"+str(aux)+";'Hoja 1'!$B:$D;3;FALSO())",#7
    	"",#8
    	tipoDato,#9
    	'=SI.ERROR(SI(ENCONTRAR("date";MINUSC($Q'+str(aux)+');1)=1;"yyyy-MM-dd";"");SI.ERROR(SI(ENCONTRAR("decimal";MINUSC($Q'+str(aux)+');1)=1;SI(LARGO($Q'+str(aux)+')=13;EXTRAE($Q'+str(aux)+';9;4);SI(LARGO($Q'+str(aux)+')=11;EXTRAE($Q'+str(aux)+';9;2);SI(LARGO($Q'+str(aux)+')=12;EXTRAE($Q'+str(aux)+';9;3);SI(LARGO($Q'+str(aux)+')=10;EXTRAE($Q'+str(aux)+';9;1))))))))', #10 FORMULA FORMATO CAMPO ORIGEN
    	'=SI(\'DC-DD-Object\'!$W$5="TERADATA";BUSCARV(MINUSC(Q'+str(aux)+');Formatos!A:B;2;FALSO());SI(\'DC-DD-Object\'!$W$5="HOST";BUSCARV(MINUSC(Q'+str(aux)+');Formatos!E:F;2;FALSO())))', #11 Formula para formato
    	"NO", #12 Formato lógico formula
    	"NO", #13
    	"", #14
    	fieldO, #15 
    	nombre_campo, #16
    	formato_campo,#17
    	'=SI.ERROR(SI(ENCONTRAR("date";MINUSC($Q'+str(aux)+');1)=1;"yyyy-MM-dd";"");SI.ERROR(SI(ENCONTRAR("decimal";MINUSC($Q'+str(aux)+');1)=1;SI(LARGO($Q'+str(aux)+')=13;EXTRAE($Q'+str(aux)+';9;4);SI(LARGO($Q'+str(aux)+')=11;EXTRAE($Q'+str(aux)+';9;2);SI(LARGO($Q'+str(aux)+')=12;EXTRAE($Q'+str(aux)+';9;3);SI(LARGO($Q'+str(aux)+')=10;EXTRAE($Q'+str(aux)+';9;1))))))))',#18
    	"",#19
    	"",#20
    	"",#21
    	"",#22
    	"",#23
    	"Mexico",#24
    	"",#25
    	"",#26
    	"",#27
    	"",#28
    	"",#29
    	"",#30
    	"",#31
    	"YES",#32
    	"",#33
    	"",#34
        ""#35
    	]
        valuesDC_DD_Field.append(data)
        i+=1
    new_file = mf.createFile(service, nombre_diccionario)
    new_file_id = new_file['spreadsheetId']
    mf.copyTo(service, PLANTILLA_ID, new_file_id, SHEET_ID1)
    mf.copyTo(service, PLANTILLA_ID, new_file_id, SHEET_ID2)
    mf.copyTo(service,PLANTILLA_ID, new_file_id, SHEET_ID3)
    spreadsheet_new_file=mf.readSpreadsheet(service, new_file_id)
    new_sheetId1=spreadsheet_new_file["sheets"][1]["properties"]["sheetId"]
    new_sheetId2=spreadsheet_new_file["sheets"][2]["properties"]["sheetId"]
    new_sheetId3=spreadsheet_new_file["sheets"][3]["properties"]["sheetId"]
    name1="DC-DD-Object"
    name2="DC-DD-Field"
    name3="Formatos"
    mf.renameSheets(service, new_file_id, new_sheetId1, name1)
    mf.renameSheets(service, new_file_id, new_sheetId2, name2)
    mf.renameSheets(service, new_file_id, new_sheetId3, name3)
    mf.updateSheetObject(service, valuesDC_DD_Object, new_file_id)
    mf.updateSheetField(service, new_file_id, mv.numero_campos, valuesDC_DD_Field, valuesDC_DD_Object)
    mf.formatSheet(service,new_file_id, new_sheetId2, mv.numero_campos)
    sheet = service.spreadsheets()
    result=[]
    try:
        print("Intentando obtener el repositorio global...")
        result = sheet.values().get(spreadsheetId=ID_GLOBAL,range=RANGE_GLOBAL).execute().get('values', [])
    except:
        print("No se pudo obtener el repositorio Global")
    valores=[]
    i=0
    if len(result)>1:
        for row in result:
            data=[row[0],row[4], row[5]]
            valores.append(data)
        mf.updateSheetAux(service,valores,new_file_id)
    else:
        print("No se pudo obtener información del Repo Global, tendrás que copiarlo manualmente. ")
    print("Renombrando Hoja 1 a Global")
    mf.renameSheets(service,new_file_id, 0, "Global")
    print("Termina generación de diccionario")
if __name__ == '__main__':
    main()