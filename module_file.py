# -*- coding: UTF-8 -*-
from __future__ import print_function
import pickle
from pprint import pprint
from googleapiclient import discovery
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import sys
def banner():
    print("  ____ _____ _   _ ____ ___ ____ _____         _   ___  ")
    print(" / ___| ____| \ | |  _ \_ _/ ___|_   _| __   _/ | / _ \ ")
    print("| |  _|  _| |  \| | | | | | |     | |   \ \ / / || | | |")
    print("| |_| | |___| |\  | |_| | | |___  | |    \ V /| || |_| |")
    print(" \____|_____|_| \_|____/___\____| |_|     \_/ |_(_)___/ ")
    print()
    print()
def getCredentials(SCOPES):
    creds = None
    try:
        if os.path.exists('token.pickle'):
            with open('token.pickle', 'rb') as token:
                creds = pickle.load(token)
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'credentials.json', SCOPES)
                creds = flow.run_local_server()
            with open('token.pickle', 'wb') as token:
                pickle.dump(creds, token)
        print("Credenciales obtenidas con éxito...")
    except:
        print("--> Ocurrió un error al intentar obtener las credenciales de tu cuenta de google.")
        print("**** Revisa si existe el archivo credentials.json en el directorio raíz del Script o verifica tu conexión a Internet")
        sys.exit()
    return creds
def createFile(service,nombreDiccionario):
	spreadsheet_body = {"properties": {"title": nombreDiccionario}}
	request = service.spreadsheets().create(body=spreadsheet_body)
	response = request.execute()
	print('Se creo el diccionario '+response['spreadsheetId'])
	return response
def readSpreadsheet(service, spreadsheetId):
	result = None
	include_grid_data = False  # TODO: Update placeholder value.
	request = service.spreadsheets().get(spreadsheetId=spreadsheetId)
	response = request.execute()
	return response
def copyTo(service, spreadsheetId, newfileId, sheet_id):
	copy_sheet_to_another_spreadsheet_request_body = {'destination_spreadsheet_id': newfileId,}
	request = service.spreadsheets().sheets().copyTo(spreadsheetId=spreadsheetId, sheetId=sheet_id, body=copy_sheet_to_another_spreadsheet_request_body)
	response = request.execute()
def deleteSheet(service, spreadsheetId, sheet_id):
	body={"requests": [{"deleteSheet": {"sheetId": sheet_id}}]}
	request = service.spreadsheets().batchUpdate(spreadsheetId=spreadsheetId, body=body)
	response = request.execute()
def renameSheets(service, spreadsheetId, sheetId, newName):
	add_tab_request = {
	"requests": [
	{
	"updateSheetProperties": {
		"properties": {
        	"sheetId": sheetId,
            "title": newName
				},
			'fields': 'title'
	},}]}
	request = service.spreadsheets().batchUpdate(spreadsheetId=spreadsheetId, body=add_tab_request)
	response = request.execute()
	return response
def updateSheetObject(service, valuesDC_DD_Object, NEW_DICTIONARY_ID ):
	rangeDictionary = 0
	if not valuesDC_DD_Object:
		print('No data found.')
		exit
	else:
		dictionaryRange = 'DC-DD-Object!A5:AI5'
		print('Rango a escribir en '  +NEW_DICTIONARY_ID+ ': ' + dictionaryRange)
		print('Comienza a escribir')
		dataPrueba = []
		dataPrueba.append(valuesDC_DD_Object)
		value_input_option = 'RAW'
		value_range_body = {
			"majorDimension": "ROWS",
			"values": dataPrueba
		   }
		request = service.spreadsheets().values().update(spreadsheetId=NEW_DICTIONARY_ID, range=dictionaryRange, valueInputOption=value_input_option, body=value_range_body)
		response = request.execute()
		print('Termina edición de hoja Object')
def updateSheetField(service, NEW_DICTIONARY_ID, numCampos, valuesDC_DD_Field, valuesDC_DD_Object):
	rangeDictionary = 0
	print("Comienza a insertar filas para Fields")
	maxCeld = int(numCampos)+4
	print (maxCeld)
	dictionaryRange = 'DC-DD-Field!A5:AI'+str(maxCeld)
	print('Rango a escribir en '  +NEW_DICTIONARY_ID+ ': ' + dictionaryRange)
	print('Comienza a escribir')
	value_input_option = 'USER_ENTERED'
	value_range_body = {
		"majorDimension": "ROWS",
		"values": valuesDC_DD_Field
	   }
	request = service.spreadsheets().values().update(spreadsheetId=NEW_DICTIONARY_ID, range=dictionaryRange, valueInputOption=value_input_option, body=value_range_body)
	response = request.execute()
	print('Termina edición de hoja Fields')
def formatSheet(service, NEW_DICTIONARY_ID, new_sheetId2, numero_campos):
    print("Da formato hoja fields")
    add_tab_request = {
	  "requests": [
	    {
	      "updateBorders": {
	        "range": {
	          "sheetId": new_sheetId2,
	          "startRowIndex": 5,
	          "endRowIndex": int(numero_campos)+5,
	          "startColumnIndex": 0,
	          "endColumnIndex": 35
	        },
	        "top": {
	          "style": "SOLID",
	          "width": 1,
	          "color": {
				"red": 0.09,
  				"green": 0.12,
  				"blue": 0.16,
	          },
	        },
	        "bottom": {
	          "style": "SOLID",
	          "width": 1,
	          "color": {
	            "red": 0.09,
  				"green": 0.12,
  				"blue": 0.16,
	          },
	        },
	        "innerHorizontal": {
	          "style": "SOLID",
	          "width": 1,
	          "color": {
	            "red": 0.09,
  				"green": 0.12,
  				"blue": 0.16,
	          },
	        },
			"innerVertical": {
	          "style": "SOLID",
	          "width": 1,
	          "color": {
	            "red": 0.09,
  				"green": 0.12,
  				"blue": 0.16,
	          },
	        },
			"left": {
	          "style": "SOLID",
	          "width": 1,
	          "color": {
	            "red": 0.09,
  				"green": 0.12,
  				"blue": 0.16,
	          },
	        },
			"right": {
	          "style": "SOLID",
	          "width": 1,
	          "color": {
	            "red": 0.09,
  				"green": 0.12,
  				"blue": 0.16,
	          },
	        },
	      }
	    }
	  ]
	}
    request = service.spreadsheets().batchUpdate(spreadsheetId=NEW_DICTIONARY_ID, body=add_tab_request)
    response = request.execute()
def updateSheetAux(service, valuesDC_DD_Object, NEW_DICTIONARY_ID ):
	rangeDictionary = 0
	if not valuesDC_DD_Object:
		print('No data found.')
		exit
	else:
		dictionaryRange = 'Hoja 1!B:G'
		print('Rango a escribir en '  +NEW_DICTIONARY_ID+ ': ' + dictionaryRange)
		print('Comienza a escribir')
		value_input_option = 'RAW'
		value_range_body = {
			"majorDimension": "ROWS",
			"values": valuesDC_DD_Object
		   }
		request = service.spreadsheets().values().update(spreadsheetId=NEW_DICTIONARY_ID, range=dictionaryRange, valueInputOption=value_input_option, body=value_range_body)
		response = request.execute()
		print('Termina edición de hoja')
def getTablaBUI(service, nombreTabla,SPREADSHEET_BUI,RANGE_BUI ):
    values = readRange(service, SPREADSHEET_BUI, RANGE_BUI).get('values', [])
    i=0
    result =[False, "", "","",""]
    while i<len(values):
        if nombreTabla.lower() == values[i][1].lower():
            print("Tabla: "+nombreTabla+" encontrada.")
            result =[True, values[i][21],values[i][7],values[i][20],values[i][6]]
        i+=1
    if result[0] == False :
        print("Tabla - "+nombreTabla+" NO encontrada en la BUI.")
    return result
def readRange(service, spreadsheetId, range):
		result = None
		try:
			result = service.spreadsheets().values().get(spreadsheetId=spreadsheetId, range=range).execute()
			return result
		except:
			print("Error: La hoja " + spreadsheetId + " no devolvio valores en el rango: " + range)
			sys.exit()