# -*- coding: UTF-8 -*-
import sys
import module_file as mf
def getNombreFiscoObjeto(persistencia, sistema_origen, nombre_tabla, aplicativo, uuaa):
	if (persistencia == "RAW" or persistencia == "MASTER") :# and (sistema_origen is "TERADATA" or sistema_origen is "ORACLE" or sistema_origen is "HOST"):
		if persistencia == "RAW":
			return (getNombreObjetoOrigenRaw(sistema_origen.upper(), nombre_tabla, aplicativo))
		else:
			return (getNombreObjetoOrigenMaster(sistema_origen.upper(), nombre_tabla, aplicativo, uuaa))
def getNombreObjetoOrigenRaw(sistema_origen, nombre_tabla, aplicativo):
	switcher = {
	"TERADATA": "t_"+aplicativo.lower()+"_"+nombre_tabla.lower(),
	"HOST": "t_"+aplicativo.lower()+"_"+nombre_tabla.lower()+"_001",
	"ORACLE": "t_"+aplicativo.lower()+"_"+nombre_tabla.lower()+"_001",
	}
	return (switcher.get(sistema_origen, "No entra el switch"))
def getNombreObjetoOrigenMaster(sistema_origen, nombre_tabla, aplicativo, uuaa):
	switcher = {
	"TERADATA": "t_"+uuaa.lower()+"_"+nombre_tabla.lower(),
	"HOST": "t_"+uuaa.lower()+"_"+nombre_tabla.lower(),
	"ORACLE": "t_"+uuaa.lower()+"_"+nombre_tabla.lower(),
	}
	return (switcher.get(sistema_origen, "No entra el switch"))
def imprimirVariablesObject(
    objectA,
    objectB,
    objectC,
    objectD,
    objectE,
    objectF,
    objectG,
    objectH,
    objectI,
    objectJ,
    objectK,
    objectL,
    objectM,):
    print("Nombre tabla: "+objectA)
    print("Nombre objeto fisico:" +objectB)
    print("Nombre lógico: "+objectC)
    print("Descripción: "+objectD)
    print(objectE)
    print(objectF)
    print(objectG)
    print(objectH)
    print(objectI)
    print(objectJ)
    print(objectK)
    print(objectL)
    print(objectM)
def getDataSourceResponsable (service, SPREADSHEET_DATASOURCE, RANGE_DATASOURCE, UUAA):
	result = [False,"",""]
        valuesDataSource = mf.readRange(service, SPREADSHEET_DATASOURCE, RANGE_DATASOURCE ).get('values', [])
	if not valuesDataSource:
		print('No data found.')
	else:
		for colum in valuesDataSource:
			if colum[1] == UUAA:
				return [True,colum[8], colum[6]]
        return result
def getAlmacenamientoTipoZonaArchivoSalida(persistencia):
	switcher = {
	"RAW": ["HDFS-Avro", "RAWDATA","Avro" ],
	"MASTER": ["HDFS-Parquet", "MASTERDATA", "Parquet"],
	}
	return (switcher.get(persistencia, ["None","None","None"]))
def getRutaOrigen(persistencia, sistema_origen, aplicativo, nombre_tabla, esquema_teradata):
    if persistencia == "RAW":
        switcher = {
        "HOST": "/in/staging/ratransmit/host/"+aplicativo,
        "ORACLE": "/in/staging/ratransmit/oracle/"+aplicativo,
        "TERADATA": "/in/staging/tpt/teradata/"+aplicativo+"/"+esquema_teradata+"."+nombre_tabla+"/[date]"
        }
        return (switcher.get(sistema_origen, ""))
    if persistencia == "MASTER":
    	switcher = {
    	"HOST": "/data/raw/ratransmit/host/"+aplicativo,
    	"ORACLE": "/in/staging/ratransmit/oracle/"+aplicativo,
    	"TERADATA": "/data/raw/"+aplicativo+"/data/"+getNombreObjetoOrigenRaw(sistema_origen, nombre_tabla, aplicativo)
    	}
    	return (switcher.get(sistema_origen, ""))
def getRutaEsquemasCurrent(persistencia, aplicativo, uuaa):
	switcher = {
	"RAW": "/data/"+persistencia.lower()+"/"+aplicativo+"/schemas/current",
	"MASTER": "/data/"+persistencia.lower()+"/"+uuaa.lower()+"/schemas/current"
	}
	return (switcher.get(persistencia, "No entra el switch"))
def getEtiqueta(nombre_tabla):
	indice = 0
	etiqueta = ""
	while indice < len(nombre_tabla):
		letra = nombre_tabla[indice]
		if letra != "_":
			etiqueta=etiqueta+letra
		indice += 1
	return etiqueta.lower()
def getRutaEsquema (service, persistencia, SPREADSHEET_ALHAMBRA, RANGE_ALHAMBRA, UUAA, aplicativo, nombreFisicoObjeto):
    valuesData = mf.readRange(service, SPREADSHEET_ALHAMBRA, RANGE_ALHAMBRA ).get('values', [])
    if persistencia.lower() == "raw":
        ruta = "/data/"+persistencia.lower()+"/"+aplicativo.lower()+"/data/"+nombreFisicoObjeto
    else:
        if persistencia.lower() == "master":
            ruta = "/data/"+persistencia.lower()+"/"+UUAA.lower()+"/data/"
    if not valuesData:
        print('No data found.')
    else:
        dim1 = len(valuesData)
        i=0
        valuesData[i][1]
        rangoPersis=0
        inicioPersis=0
        while (dim1-1) > i:
            try:
                if valuesData[i][1] == persistencia.lower() or valuesData[i][1] == '':
                    if valuesData[i][1] == persistencia.lower():
                        inicioPersis=i
                else:
                    if rangoPersis != 0:
                        exit
                    else:
                        rangoPersis=i
                i+=1
            except:
                i+=1
        if persistencia.lower() == "master":
            i=inicioPersis
            rangoUUAA=0
            iniciaUUAA=0
            found = False
            while (i<rangoPersis):              
                try:
                    if valuesData[i][2] == UUAA.lower() or valuesData[i][2] == '':
                        if valuesData[i][2] == UUAA.lower():
                            iniciaUUAA=i
                            found=True
                    else:
                        if rangoUUAA != 0:
                            i=rangoPersis
                            exit
                        else:
                            if found==True:
                                rangoUUAA=i
                    i+=1
                except:
                    i+=1
            if rangoUUAA-iniciaUUAA <=2:
                print("No hay valores de dominio")
                ruta = ruta+nombreFisicoObjeto
                exit
            else:
                i=(iniciaUUAA+1)
                while (i<rangoUUAA):
                    try:
                        ruta=ruta+"***"+valuesData[i][4]+"***"
                        i+=1
                    except:
                        i+=1
                ruta=ruta+"/"
    return ruta