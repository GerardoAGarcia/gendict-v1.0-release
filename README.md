# GenDict V1.0 Release

## Introduction

>This Script let you generate a new spreadsheet in your own Drive with previus parameters
The diccionario generated <strong> is only a guide </strong> but it need some modification for be sended to Governance.

## Code Samples

> The source code is in the next address <br>
<strong>https://gitlab.com/GerardoAGarcia/gendict-v1.0-release.git </strong><br>
You can clone this 


## Installation

>--> To run this script you need to have Python 2.7 version installed or higher.<br>
>--> You need to have the <strong>credentials.json</strong> file in the script local path or <strong>token.pickle</strong>
> This is an example from script execution: <br>
><strong> python GenDict.py -t nombre_tabla -s teradata -p master -a cn -u mdco -st xpa -sql </strong><br>
> The allow arguments are:
-t table_name (obligatory) <br>
-s source_sysmte (obligatory)<br>
-p persistency (obligatory)<br>
-f frecuency <br>
-a aplication (obligatory)<br>
-u uuaa \n -sql SQL_file ( t or f)<br>
-st teradata_schema (only when the source system is Teradata)<br>

### Credits
By Gerardo A Garcia (Jerry)